let registerForm = document.querySelector("#registerForm");

registerForm.addEventListener("submit", (e) => {

	e.preventDefault();

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNumber = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value


	console.log(firstName)
	console.log(lastName)
	console.log(mobileNumber)
	console.log(email)
	console.log(password1)
	console.log(password2)

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

		/*

			fetch is a built in js function that allows us to get data frm another source without the need to refresh the page.
			This aalows to get a response if an email we are trying to register has already been registered in our database

			fetch sends the data to the url provided with its paramteres:

			fetch(<url>,<parameter>)

			paramters may contain:
			//method -> http method(should reflect the http method as defined in your backend)
			//headers 
				-> Content-Type - defines what kind of data to send.
				-> authorization - contains our Bearer Token
			//body -> the body of our request or req.body

		*/


		fetch('https://morning-brushlands-50621.herokuapp.com/api/users/email-exists', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})

		})

		.then(res => res.json())
		//data is the response now in a json format.
		.then(data => {

			//data => true or false
			//if true, then the email already exists in our database.
			//if false, then the email has yet to be regisdtered/

			//this will check if the email exists or not, if the email exists, then will register the user, if not, we're going to show an alert.
			if(data === false){

				//nest a fetch request using the registration to register our user, if the email being regustered does not already exists in our database.
				fetch('https://morning-brushlands-50621.herokuapp.com/api/users/',{

					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNumber

					})

				})

				.then(res => res.json())
				.then(data => {

					if(data === true){

						alert("Registration Successful")
						//this method allows to replace the current document with the document provide in the method which means that for a successful registration we will be 
						window.location.replace("./login.html")

					} else {

						alert("Registration Failed")

					}

						

				})

			} else {

				alert("Email Already Exists.")

			}
			
		})

	} else {

		alert('Invalid Input.')

	}


})