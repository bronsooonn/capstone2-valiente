let navItems = document.querySelector("#navSession");

let userId = localStorage.getItem('id')

console.log(userId)

let userToken = localStorage.getItem("token");

let isAdmin = localStorage.getItem('isAdmin')


let loginButton = document.querySelector("#login-button")

if(!userToken) {

	navItems.innerHTML += 
	`
		<li class="nav-item active">
			<a href="./login.html" class="nav-link"> Login </a>
		</li>
		<li class="nav-item active">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`

	
} else if(userToken && isAdmin === "false"){
	
	fetch('https://morning-brushlands-50621.herokuapp.com/api/users/')
	.then(res => res.json())
	.then(user => {
		navItems.innerHTML += 
		`
			<li class="nav-item active">
				<a href="./profile.html?userId=${userId}" class="nav-link"> Profile </a>
			</li>
			<li class="nav-item active">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>

		`
		})

}	else {
	navItems.innerHTML += 
	`
		<li class="nav-item active">
			<a href="./logout.html" class="nav-link"> Logout </a>
		</li>

	`
}