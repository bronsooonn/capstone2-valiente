//We use URLSearchParams to access the specific parts of query string in the URL
//d1/pages/deleteCourse.html?courseId=5fbb4c642870f80cbca816d7
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

//how do we get the token from the localStorage?
let token = localStorage.getItem("token")

//this fetch will be run automatically once you get the page deleteCourse page.
fetch(`https://morning-brushlands-50621.herokuapp.com/api/courses/activate/${courseId}`, {

	method: 'PUT',
	headers: {

		"Authorization": `Bearer ${token}`

	}

})
.then(res => res.json())
.then(data => {

	if(data){
		alert("Course Activated")
	}else{
		alert("Something Went Wrong")
	}

})