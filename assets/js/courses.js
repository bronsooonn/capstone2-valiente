let adminUser = localStorage.getItem('isAdmin')
let addButton = document.querySelector('#adminButton')

let cardFooter //will allow us to add a button at the bottom of each of our courses which can redirect to the specific course.

let token = localStorage.getItem('token')


if(adminUser === "true"){

	addButton.innerHTML += 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-primary"> Add Course </a> 
		</div>
	`
	fetch('https://morning-brushlands-50621.herokuapp.com/api/courses/allcourses', {

		headers: {
			"Authorization": `Bearer ${token}`
		}

	})
	.then(res => res.json())
	.then(data => {

		let courseData;

		//to check if there are any active courses
		if(data.length < 1){

			courseData = "No Courses Available"

		} else {

			courseData = data.map(course => {

				let isActive = course.isActive
				console.log(course.length)

				//conditionally render the content of our cardFooter with buttons that only an admin can see or a button that only a regular user can see.

				if(adminUser == "false" || !adminUser){

					cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block">Go to Course</a>`

				} else if(adminUser == "true" && isActive === false){ //if current user is an admin

					//button to archive/deactivate
					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block">Go to Course</a>
						<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>
					`

				} else if(adminUser == "true" && isActive === true){ //if current user is an admin

					//button to archive/deactivate
					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block">Go to Course</a>
						<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Courses</a>
					`

				}

				//will be return as each item in the new array
				return (

						`
							<div class="col-md-12 my-3">
								<div class= "card">
									<div class="card-body bg-white">
										<h5 class="cart-title"><strong>${course.name}</strong></h5>
										<p class="card-text text-left">${course.description}</p>
										<p id="priceCourses" class="card-text text-right">Price: ₱${course.price}</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)
			}).join("")

		}


		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData

	})

} else {

	fetch('https://morning-brushlands-50621.herokuapp.com/api/courses/')
	.then(res => res.json())
	.then(data => {

		console.log(data)

		let courseData;

		//to check if there are any active courses
		if(data.length < 1){

			courseData = "No Courses Available"

		} else {

			courseData = data.map(course => {

				let isActive = course.isActive

				//conditionally render the content of our cardFooter with buttons that only an admin can see or a button that only a regular user can see.

				if(adminUser == "false" || !adminUser){

					cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block">Go to Course</a>`

				} else if(adminUser == "true" && isActive === false){ //if current user is an admin

					//button to archive/deactivate
					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block">Go to Course</a>
						<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Activate Course</a>
					`

				} else if(adminUser == "true" && isActive === true){ //if current user is an admin

					//button to archive/deactivate
					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" class="btn btn-info text-white btn-block">Go to Course</a>
						<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Courses</a>
					`

				}

				//will be return as each item in the new array
				return (

						`
							<div class="col-md-12 my-3">
								<div class= "card">
									<div class="card-body">
										<h5 class="cart-title"><strong>${course.name}</strong></h5>
										<p class="card-text text-left">${course.description}</p>
										<p class="card-text text-right">Price: ₱${course.price}</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)
			}).join("")

		}


		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData


	})

}
